// -*- mode:c++ -*-

  
module createContainer{

  exception AlreadyExists {string key;};
  exception NoSuchKey {string key;};

  dictionary <int, Object*> ObjectPrxDict;

  interface Container{
    void link(int key, Object* proxy) throws AlreadyExists;
    void unlink(int key) throws NoSuchKey;
    ObjectPrxDict list();
    int size();
  };
};

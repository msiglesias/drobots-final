#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Marcos Sanchez Iglesias
#Sergio Perez Sanchez

import sys
import Ice
import socket

Ice.loadSlice('drobots.ice')
import drobots

Ice.loadSlice('FactoryController.ice --all -I .')
import RobotControllerFactory

Ice.loadSlice('Container.ice')
import createContainer

from RobotController import *




class FactoryI(RobotControllerFactory.Factory, Ice.Application):
    container = None
    broker = None

    def __init__(self):
        self.IP = self.getIP()
        self.broker = self.communicator()
        containerPrx = self.broker.stringToProxy("ContainerControllers -t -e 1.1:tcp -h " + self.IP + " -p 9999 -t 60000")
        self.container = createContainer.ContainerPrx.checkedCast(containerPrx)

    def getIP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("atclab.esi.uclm.es", 8080))
        IP = s.getsockname()[0]

        return IP

    def make(self, robot, contador, current=None):
        print("Creando controladores en factoria")


        if (robot.ice_isA("::drobots::Attacker") and robot.ice_isA("::drobots::Defender")):
            print("completo")
            robotControllerSirviente = RobotControllerRobotI(robot)
            robotControllerProxy = current.adapter.addWithUUID(robotControllerSirviente)

            robotController = drobots.RobotControllerPrx.uncheckedCast(robotControllerProxy)
            self.container.link(contador, robotController)

        elif robot.ice_isA("::drobots::Attacker"):
            print ("Atacante")

            robotControllerSirviente = RobotControllerAttackerI(robot, self.container)
            robotControllerProxy = current.adapter.addWithUUID(robotControllerSirviente)

            robotController = drobots.RobotControllerPrx.uncheckedCast(robotControllerProxy)
            self.container.link(contador, robotController)


        else:
            """robot.ice_isA("::drobots::Defender")"""

            print ("Defensor")
            robotControllerSirviente = RobotControllerDefenderI(robot, self.container)
            robotControllerProxy = current.adapter.addWithUUID(robotControllerSirviente)

            robotController = drobots.RobotControllerPrx.uncheckedCast(robotControllerProxy)
            self.container.link(contador, robotController)


        return robotController





class FactoryController(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = FactoryI()

        adapter = broker.createObjectAdapter("FactoryControllerAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("Factory"))

        print(proxy)

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

factory = FactoryController()
sys.exit(factory.main(sys.argv))

#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Marcos Sanchez Iglesias
#Sergio Perez Sanchez

import sys
import Ice

Ice.loadSlice('drobots.ice')
import drobots
from random import randint
import math

Ice.loadSlice('Container.ice')
import createContainer

Ice.loadSlice('FactoryController.ice --all -I .')
import RobotControllerFactory

class RobotControllerI(drobots.RobotController):
    pass

class RobotControllerAttackerI(RobotControllerFactory.Attacker, Ice.Application):

    def __init__(self, robot, container):
        self.robot=robot
        self.xa = self.robot.location().x
        self.ya = self.robot.location().y
        self.enemigos = 0
        self.incremento = 20
        self.xd = 0
        self.yd = 0
        self.container = container
        self.contador=0




    def turn(self, current=None):

        if(self.enemigos != 0):
            diferenciay = self.yd - self.ya
            distancia = math.sqrt(((self.ya - self.yd) ** 2) + ((self.xa - self.xd) ** 2))


            self.angulo = math.degrees(math.asin(diferenciay / distancia))


            if (self.xa < 200 and self.ya < 200):
                if (self.xa > self.xd):
                    if (self.ya > self.yd):
                        self.robot.cannon(abs(self.angulo) + 180, distancia)
                    else:
                        self.robot.cannon((self.angulo * -1) + 180, distancia)
                else:
                    self.robot.cannon(self.angulo, distancia)

            elif (self.xa > 199 and self.ya < 200):
                if (self.xa > self.xd):
                    if (self.ya > self.yd):
                        self.robot.cannon(abs(self.angulo) + 180, distancia)
                    else:
                        self.robot.cannon((self.angulo * -1) + 180, distancia)
                else:
                    self.robot.cannon(self.angulo, distancia)

            elif (self.xa < 200 and self.ya > 199):
                if (self.xa > self.xd):
                    if (self.ya > self.yd):
                        self.robot.cannon(abs(self.angulo) + 180, distancia)
                    else:
                        self.robot.cannon((self.angulo * -1) + 180, distancia)
                else:
                    self.robot.cannon(self.angulo, distancia)

            elif (self.xa > 199 and self.ya > 199):
                if (self.xa > self.xd):
                    if (self.ya > self.yd):
                        self.robot.cannon(abs(self.angulo) + 180, distancia)
                    else:
                        self.robot.cannon((self.angulo * -1) + 180, distancia)
                else:
                    self.robot.cannon(self.angulo, distancia)

        else:
            self.robot.cannon(randint(0,359), randint(41, 200))


        self.enemigos = 0




    def alert(self, enemigos, direccion, coordenadaX, coordenadaY, current=None):
        self.enemigos = enemigos
        self.xd = coordenadaX + (math.cos(math.radians(direccion)) * self.incremento)
        self.yd = coordenadaY + (math.sin(math.radians(direccion)) * self.incremento)
        self.incremento += 20

        if(self.incremento > 200):
            self.incremento = 20
            self.enemigos=0

    def alertDetector(self, enemigos, coordenadaX, coordenadaY, current=None):
        self.contador+=1
        self.enemigos = enemigos
        self.xd = coordenadaX
        self.yd = coordenadaY
        if(self.contador>=2):
            self.enemigos=0

    def robotDestroyed(self, current=None):
        print("Atacante destruido")


class RobotControllerDefenderI(RobotControllerFactory.Defender,  Ice.Application):




    def __init__(self, robot, container):
        self.robot=robot
        self.contador = 0
        self.container = container
        self.enemigos = 0
        self.direccion = randint(0,359)
        self.flag = 0
        self.atacar=0




    def turn(self, current=None):

        #self.enemigos = 0
        self.contador = 0
        while (self.enemigos == 0 and self.contador < 8):
            print("Escaneando "+str(self.direccion))
            self.enemigos = self.robot.scan(self.direccion, 20)


            self.direccion += 10

            self.contador+=1

            if (self.direccion >= 360):
                self.direccion -= 360

        if(self.enemigos > 0):
            robots = self.container.list()
            atacante = RobotControllerFactory.AttackerPrx.uncheckedCast(robots[1])
            atacante.alert(self.enemigos, self.direccion, self.robot.location().x, self.robot.location().y)
            self.atacar+=1
            if(self.atacar==10):
                self.atacar=0
                self.enemigos=0
                self.direccion+=10

    def resetEnemigos(self, current=None):
        """print("Reset")
        self.enemigos=0
        self.direccion+=10"""

    def robotDestroyed(self, current=None):
        print("Defensor destruido")


class RobotControllerRobotI(RobotControllerFactory.Robot, Ice.Application):
    enemigos=0

    def __init__(self, robot):
        self.robot=robot
        self.distancia=0
        #self.enemigos=0
        self.angulo=0
        self.incremento=100


    def turn(self, current=None):

        if(self.enemigos!=0):
            self.robot.cannon(self.angulo, self.incremento)
            self.incremento+=20
            if (self.incremento>=200):
                self.incremento=80
                self.enemigos=0
                self.angulo+=20

        else:
            while(self.robot.energy()>10 and self.enemigos==0):
                self.enemigos = self.robot.scan(self.angulo, 20)
                if (self.enemigos == 0):
                    self.angulo += 20






    def robotDestroyed(self, current=None):
        print("Robot destruido")



#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Marcos Sanchez Iglesias
#Sergio Perez Sanchez

import sys
import Ice

Ice.loadSlice('drobots.ice')
import drobots
import random
import math

Ice.loadSlice('FactoryController.ice --all -I .')
import RobotControllerFactory

Ice.loadSlice('Container.ice')
import createContainer



class DetectorController(drobots.DetectorController, Ice.Application):

    def __init__(self, container):
        self.x = 0
        self.y = 0
        self.enemies = 0
        self.container = container

    def alert(self, point, enemies, current=None):
        self.x = point.x
        self.y = point.y
        self.enemies = enemies

        robots = self.container.list()
        atacante1 = RobotControllerFactory.AttackerPrx.uncheckedCast(robots[1])
        atacante1.alertDetector(self.enemies, self.x, self.y)
        #atacante1 = RobotControllerFactory.AttackerPrx.uncheckedCast(robots[2])
        #atacante2 = RobotControllerFactory.AttackerPrx.uncheckedCast(robots[3])
        #atacante1.alertDetector(self.enemies, self.x, self.y)
        #atacante2.alertDetector(self.enemies, self.x, self.y)

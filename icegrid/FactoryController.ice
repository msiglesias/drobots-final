// -*- mode:c++ -*-

#include <drobots.ice>

module RobotControllerFactory{

  

  interface Factory{
    drobots::RobotController* make(drobots::Robot* robot, int contador);
  };

  interface FactoryDetector{
    drobots::DetectorController* makeDetector(int contadorDetector);
  };

  interface Attacker extends drobots::RobotController {
    void alert(int enemigos, int direccion, int coordenadaX, int coordenadaY);
    void alertDetector(int enemigos, int coordenadaX, int coordenadaY);

  };

  interface Defender extends drobots::RobotController {
    void resetEnemigos();

  };

  interface Robot extends drobots::RobotController {

  };

};


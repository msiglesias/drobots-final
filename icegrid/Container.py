#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Marcos Sanchez Iglesias
#Sergio Perez Sanchez
import sys
import Ice

Ice.loadSlice('Container.ice')
import createContainer



class ContainerI(createContainer.Container):
    def __init__(self, current=None):
        self.proxies = dict()

    def link(self, key, proxy, current=None):
        self.proxies[key] = proxy

    def unlink(self, key, current=None):
        del self.proxies[key]

    def list(self, current=None):
        return self.proxies

    def size(self, current=None):
        return len(self.proxies)


class ServerContainer(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        adapter = broker.createObjectAdapter("ContainerAdapter")
        servant = ContainerI()
        proxy = adapter.add(servant, broker.stringToIdentity("ContainerControllers"))

        print(proxy)

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


container = ServerContainer()
sys.exit(container.main(sys.argv))

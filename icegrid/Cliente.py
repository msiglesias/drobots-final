#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Marcos Sanchez Iglesias
#Sergio Perez Sanchez


import sys
import Ice
import socket
from random import randrange

Ice.loadSlice('drobots.ice')
import drobots

Ice.loadSlice('FactoryController.ice --all -I .')
import RobotControllerFactory



class PlayerI(drobots.Player):
    def __init__(self, ID, broker, adaptador):
        self.id = ID
        self.contador = 0
        self.key = 0
        self.contadorDetector = 0
        self.broker = broker
        self.adaptador = adaptador
        self.IP = self.getIP()

    def getIP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("atclab.esi.uclm.es", 8080))
        IP = s.getsockname()[0]

        return IP

    def makeController(self, robot, current=None):
        print("Creando Controllers")
        stringPrx = "Factory -t -e 1.1:tcp -h "+ self.IP +" -p 909"+str(self.contador)+" -t 60000"

        proxy = self.broker.stringToProxy(stringPrx)
        factory = RobotControllerFactory.FactoryPrx.checkedCast(proxy)

        if not factory:
            raise RuntimeError("Invalid proxy")

        robotController = factory.make(robot, self.key)
        self.contador += 1
        self.key += 1
        if self.contador == 3:
            self.contador = 0

        print(robotController)

        return robotController

    def makeDetectorController(self, current=None):
        print("Creando DetectorControllers")

        stringPrx = "FactoryDetector -t -e 1.1:tcp -h " + self.IP + " -p 908" + str(self.contadorDetector) + " -t 60000"
        proxy = self.broker.stringToProxy(stringPrx)
        factory = RobotControllerFactory.FactoryDetectorPrx.checkedCast(proxy)

        if not factory:
            raise RuntimeError("Invalid proxy")

        detectorController = factory.makeDetector(self.contadorDetector)
        self.contadorDetector += 1

        print(detectorController)

        return detectorController


    def win(self, current=None):
        print("Ganas la partida")
        current.adapter.getCommunicator().shutdown()

    def lose(self, current=None):
        print("Todos los robots han sido eliminados")
        current.adapter.getCommunicator().shutdown()

    def gameAbort(self, current=None):
        print("Abortaaaaaa")
        current.adapter.getCommunicator().shutdown()


class Cliente(Ice.Application):
    def run(self, args):
        #crear adaptador
        broker = self.communicator()
        adaptador = broker.createObjectAdapter("Adaptador") #mismo nombre que en .config
        adaptador.activate()
        #crear sirviente
        ID = "player" + str(randrange(100))
        sirvientePlayer = PlayerI(ID, broker, adaptador)
        proxyPlayer = adaptador.add(sirvientePlayer, broker.stringToIdentity(ID))
        print("Proxy Player: "+ str(proxyPlayer))

        dirGame = args[1]
        proxyGame = broker.stringToProxy(dirGame)
        print("Proxy Game: "+ str(proxyGame))

        player = drobots.PlayerPrx.uncheckedCast(proxyPlayer)
        game = drobots.GamePrx.checkedCast(proxyGame)

        if not game:
            raise RuntimeError("Error Proxy")

        try:
            game.login(player, ID)
            print("Comienza la partida")

        except drobots.InvalidProxy:
            print("Proxy no valido")
            return 1
        except drobots.InvalidName:
            print("Nombre no valido")
            return 1
        except drobots.GameInProgress:
            print("Partida ya en juego")
            return 1

        sys.stdout.flush()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

sys.exit(Cliente().main(sys.argv))

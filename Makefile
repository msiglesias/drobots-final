#!/usr/bin/make -f
# -*- mode:makefile -*-

factory1:
	./FactoryController.py --Ice.Config=FactoryController1.config
	
factory2:
	./FactoryController.py --Ice.Config=FactoryController2.config
	

factory3:
	./FactoryController.py --Ice.Config=FactoryController3.config
	

factoryDetector1:
	./FactoryDetectorController.py --Ice.Config=FactoryControllerDetector1.config

factoryDetector2:
	./FactoryDetectorController.py --Ice.Config=FactoryControllerDetector2.config

factoryDetector3:
	./FactoryDetectorController.py --Ice.Config=FactoryControllerDetector3.config

factoryDetector4:
	./FactoryDetectorController.py --Ice.Config=FactoryControllerDetector4.config

container:
	./Container.py --Ice.Config=Container.config
	

run:
	./Cliente.py "drobots12" --Ice.Config=Cliente.config

cliente:
	./ClienteQuieto.py "drobots12" --Ice.Config=Cliente.config

all-factories:
	make -j factory1 factory2 factory3 factoryDetector1 factoryDetector2 factoryDetector3 factoryDetector4

all:
	make -j container all-factories

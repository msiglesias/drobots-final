#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Marcos Sanchez Iglesias
#Sergio Perez Sanchez

import sys
import Ice
import socket

Ice.loadSlice('drobots.ice')
import drobots

Ice.loadSlice('FactoryController.ice --all -I .')
import RobotControllerFactory

Ice.loadSlice('Container.ice')
import createContainer

from DetectorController import *




class FactoryDetectorI(RobotControllerFactory.FactoryDetector, Ice.Application):

    def __init__(self):
        self.IP = self.getIP()
        self.broker = self.communicator()
        containerPrx = self.broker.stringToProxy("ContainerControllers -t -e 1.1:tcp -h " + self.IP + " -p 9999 -t 60000")
        self.container = createContainer.ContainerPrx.checkedCast(containerPrx)

    def getIP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("atclab.esi.uclm.es", 8080))
        IP = s.getsockname()[0]

        return IP

    def makeDetector(self, contadorDetector, current=None):
        detectorControllerSirviente = DetectorController(self.container)
        detectorControllerProxy = current.adapter.addWithUUID(detectorControllerSirviente)

        detectorController = drobots.DetectorControllerPrx.uncheckedCast(detectorControllerProxy)

        return detectorController




class FactoryDetectorController(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = FactoryDetectorI()

        adapter = broker.createObjectAdapter("FactoryDetectorControllerAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("FactoryDetector"))

        print(proxy)

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

factory = FactoryDetectorController()
sys.exit(factory.main(sys.argv))
